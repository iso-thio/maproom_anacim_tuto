#!/bin/sh -e

config="${1:-config.lua}"
languages=`miconf -c "$config" -e "print(table.concat(map(function(vs) return table.concat(vs,',') end, Languages), ' '))" -m /dev/null /dev/null`
exclude='/\.git'
dir='.'

for langs in $languages; do
   lang=${langs%%,*};
   miconf-traverse "$dir" '' '\.m$' ".t.$lang" "$exclude" "miconf-multi %s %s "$langs"";
done

for langs in $languages; do
   lang=${langs%%,*};
   miconf-traverse "$dir" '' "\\.t\\.$lang\$" ".$lang" "$exclude" "miconf -e \"configlua_filename=\\\"$config\\\";Language=\\\"$lang\\\"\" -c \"$config\" %s %s";
done

miconf-traverse "$dir" '' '\.t$' '' "$exclude" "miconf -e \"configlua_filename=\\\"$config\\\"\" -c \"$config\" %s %s"

for langs in $languages; do
   lang=${langs%%,*};
   miconf-traverse "$dir" '' "\\.(x?)jade\\.($lang)\$" '.\1html.\2' "$exclude" "jade --path ./include --pretty <%s >%s";
done

miconf-traverse "$dir" '' '\.(x?)jade$' '.\1html' "$exclude" "jade --path ./include --pretty <%s >%s"

. ./buildenv.sh
