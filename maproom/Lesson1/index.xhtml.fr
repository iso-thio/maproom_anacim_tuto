<!DOCTYPE HTML>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:wms="http://www.opengis.net/wms#"
      xmlns:iridl="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#"
      xmlns:maproom="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#"
      xmlns:term="http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl#"
      xmlns:xs="http://www.w3.org/2001/XMLSchema#"
      version="XHTML+RDFa 1.0"
 xml:lang="fr"
      >
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>Comment créer une page de Maproom et que faire avec ?</title>
<link rel="stylesheet" type="text/css" href="../../uicore/uicore.css" />
<link rel="stylesheet" type="text/css" href="../../localconfig/ui.css" />
<link class="altLanguage" rel="alternate" hreflang="en" href="index.html?Set-Language=en" />
<link rel="canonical" href="index.html" />
<link class="carryLanguage" rel="home" href="http://www.anacim.sn/" title="ANACIM" />
<link class="carryLanguage" rel="home alternate" type="application/json" href="/maproom/navmenu.json" />
<meta property="maproom:Sort_Id" content="a01" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#climate" />
<link rel="term:icon" href="http://213.154.77.59/SOURCES/.ANACIM/.ENACTS/.version1/.ALL/.monthly/.climatologies/.precip/SOURCES/.Features/.Political/.Senegal_Adm/a:/.Department/.the_geom/:a:/.Region/.the_geom/:a/X/Y/fig-/colors/thin/black/stroke/thinnish/stroke/-fig//antialias/true/psdef//T/6.5/plotvalue+//color_smoothing+1+psdef//plotborder+0+psdef//plotaxislength+432+psdef//antialias+true+psdef+.gif" />
<script type="text/javascript" src="../../uicore/uicore.js"></script>
<script type="text/javascript" src="../../localconfig/ui.js"></script>

<style>
filestyle {font-family:"monospace"; font-size:"11pts";}
dirstyle {font-family:"monospace"; font-size:"11pts"; color: blue}
bluehtml {color: blue;}
greenhtml {color: green;}
darkcyanhtml {color: DarkCyan;}
uicpink {color: HotPink;}
orangehtml {color: orange;}
</style>

</head>
<body>

<form name="pageform" id="pageform">
<input class="carryup carryLanguage" name="Set-Language" type="hidden" />
<input class="carryup titleLink itemImage" name="bbox" type="hidden" />
</form>
<div class="controlBar">
           <fieldset class="navitem">
                <legend>Data Library</legend>
                      <a rev="section" class="navlink carryup" href="/maproom/">Ma Première Maproom</a>
            </fieldset>
           <fieldset class="navitem">
                <legend>Ma Première Maproom</legend>
                     <span class="navtext">Leçon 1 section</span>
            </fieldset>
            <fieldset class="navitem">
                <legend>Région</legend>
            <a class="carryLanguage" rel="iridl:hasJSON" href="/maproom/senegalregions.json"></a>
                <select class="RegionMenu" name="bbox">
                <option value="">Sénégal</option>
                <optgroup class="template" label="Région">
                <option class="irigaz:hasPart irigaz:id@value term:label"></option>
                </optgroup>
                </select>
            </fieldset>

 </div>
<div>
 <div id="content" class="searchDescription">
<h2 property="term:title">Comment créer une page de Maproom et que faire avec ?</h2>
<p align="left" property="term:description">
Ce tutoriel va vous enseigner comment créer une page dans une Maproom et ce que vous pouvez y faire.
</p>
<p>
Pour le moment, Leçon 1 est une section de Maproom vide. C'est-à-dire qu'il n'y a pas de sous-sections ou de pages individuelles dedans. Cependant, dans le répertoire <dirstyle>Lesson1/</dirstyle>, nous disposons effectivement de fichiers <filestyle>Lesson1.html.lg</filestyle>. <filestyle>lg</filestyle> dénote une langue telle que <filestyle>fr</filestyle> pour le français ou <filestyle>en</filestyle> pour l'anglais et vous pouvez choisir de travailler avec la langue de votre choix.
</p>
<p>
Il y a trois types de fichiers dans un projet de Maproom :
<ul>
<li><filestyle>.xhtml.lg</filestyle> ;</li>
<li><filestyle>.html.lg</filestyle> ;</li>
<li>autres fichiers.</li>
</ul>
Nous verrons au fur et à mesure à quoi servent les "autres fichiers". Les fichiers <filestyle>.html.lg</filestyle> sont les pages de Maproom que nous souhaitons créer. Cependant, un site internet de Maproom est virtuel avant qu'il ne soit construit (par la commande <filestyle>make</filestyle>). Les fichiers <filestyle>.xhtml.lg</filestyle> sont responsables de la structure finale du site de Maproom en récupérant des informations des fichiers <filestyle>.html.lg</filestyle> et les combinant avec les informations des fichiers <filestyle>.xhtml.lg</filestyle>, pour créer des nouveaux fichiers HTML temporaires qui construisent les sections et sous-sections des Maprooms et les liens de navigation entre eux. Vous pouvez considérer les pages <filestyle>.html.lg</filestyle> comme les feuilles d'un arbre, et les fichiers <filestyle>.xhtml.lg</filestyle> comme le tronc et les branches de l'arbre. Sauf que la structure du tronc et des branches n'est pas définitive mais virtuelle.
</p>
<p>
Le processus de construction du site web des Maprooms a plusieurs intérêts :
<ul>
<li>Une fois que vous aurez des douzaines et des douzaines de pages de Maprooms, comme à l'IRI, cette étape supplémentaire facilitera la création de nouvelles (sous-) sections, pages, ou leur déplacement dans le site web;</li>
<li>Bien que la construction, une fois faite, fige le site web, le système permet de construire le site dynamiquement. Par exemple en utilisant une interface de recherche ou de filtrage par mot-clés, le site des Maprooms peut être construit <i>live</i>, sur la base des choix de l'utilisateur (par exemple les Maprooms correspondant à "précipitations" en "Afrique"), et d'un champ sémantique pré-installé dans la Data Library et ses Maprooms qui permet d'étiqueter données et Maprooms.</li>
</ul>
</p>
<p>
Donc tout d'abord nous devons éditer <filestyle>Lesson1.html.lg</filestyle> pour que la page soit construite correctement dans notre section. Ouvrir <filestyle>Lesson1.html.lg</filestyle> avec votre éditeur préféré. Il y a trois éléments à considérer pour finaliser la construction d'une page de Maproom (ou d'une sous-section) sous un onglet d'une section :
<ul>
<li>Une étiquette sémantique présente à la fois dans la section <i>parent</i> dans laquelle on souhaite construire et dans la page ou sous-section <i>enfant</i> à ajouter. Ouvrir le fichier <filestyle>index.xhtml.lg</filestyle> avec votre éditeur et rechercher <uicpink>tabbedentries</uicpink> typiquement vers la fin du fichier. Vous trouverez la référence sémantique suivante :
<xmp>
<link rel="maproom:tabterm" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Lesson1" />
</xmp>
La copier et retourner au fichier <filestyle>Lesson1.html.lg</filestyle>. La coller sous la balise <bluehtml>head</bluehtml> du fichier HTML et remplacer <uicpink>maproom:tabterm</uicpink> par <uicpink>term:isDescribedBy</uicpink>.
</li>
<li>Ensuite, la construction du site (commande <filestyle>make</filestyle>) collecte trois éléments du fichier <i>enfant</i> pour créer le lien dans le fichier <i>parent</i> (xhtml) vers la page <i>enfant</i> : un titre, une description et un icône. La commande de construction va rechercher dans le fichier <i>enfant</i> un paragraphe avec l'attribut <orangehtml>property=</orangehtml><uicpink>"term:title"</uicpink>. Cela est déjà attribué et vous pouvez vérifier que le texte associé est "Leçon 1". La description est récupérée dans le paragraphe qui porte l'attribut <orangehtml>property=</orangehtml><uicpink>"term:description"</uicpink>. Cela est aussi déjà attribué. Finalement, nous avons besoin d'une icône qui peut être un gif de la DL ou tout autre image. Pour le moment, utilisons une image non-DL qui est disponible dans le répertoire <dirstyle>icons</dirstyle> de notre projet. Ajouter la ligne suivante sous la balise <bluehtml>head</bluehtml> de <filestyle>Lesson1.html.lg</filestyle> :
<xmp>
<link rel="term:icon" href="/maproom/icons/Flaticon_5169-32.png" />
</xmp>
</li>
<li>Deux balises <bluehtml>fieldsets</bluehtml> qui mettent en place dans la Barre de Contrôle des Maprooms le lien de navigation de retour vers la page <i>parent</i> et les liens de navigation vers d'éventuelles d'autres pages de la même section. Ces éléments de navigation doivent être présents pour assurer l'intégrité du site (éviter de créer des pages "cul-de-sac"). Pour les mettre en place, copier les lignes suivantes et les coller sous la balise <bluehtml>div</bluehtml> qui porte la classe <uicpink>controlBar</uicpink> dans votre fichier <filestyle>Lesson1.html.lg</filestyle> :
<xmp>
<fieldset class="navitem" id="toSectionList">
<legend>Maproom</legend>
<a rev="section" class="navlink carryup" href="/maproom/Lesson1/">Leçon 1 lien</a>
</fieldset>

<fieldset class="navitem" id="chooseSection">
<legend about="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Lesson1">
<span property="term:label">Leçon 1 label</span>
</legend>
</fieldset>
</xmp>
Le premier <bluehtml>fieldset</bluehtml> avec la légende "Maproom" met en place un lien intitulé "Leçon 1 lien" qui retourne à <dirstyle>maproom/Lesson1/</dirstyle>. Le second <bluehtml>fieldset</bluehtml> utilise la même référence sémantique que nous avons trouvé sous <uicpink>tabbedentries</uicpink> et il met en place un menu pour naviguer vers les pages de Maprooms d'une même section (le cas échéant), et libellant cet onglet "Leçon 1 label".
</li>
</ul>
Avant de reconstruire le site avec notre nouvelle Maproom, nous devons opérer les étapes ci-dessus pour toutes les autres langues disponibles (ici l'anglais). Une fois le fichier <filestyle>Lesson1.html.en</filestyle> éditer, sauvegarder vos changements (<filestyle>commit</filestyle> et <filestyle>push</filestyle> si vous le souhaitez) et construire (commande <filestyle>make</filestyle>) la Maproom à nouveau.
</p>

<h3>Récapitulation des Étapes</h3>
<p>Si vous avez déjà suivi cette leçon, la suite de tâches est :
<ol>
<li>
Ouvrir le fichier <filestyle>index.xhtml.lg</filestyle> avec votre éditeur préféré.
</li>
<li>
Chercher <uicpink>tabbedentries</uicpink> typiquement à la fin du fichier. Trouver la référence sémantique suivante :
<xmp>
<link rel="maproom:tabterm" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Lesson1" />
</xmp>
</li>
<li>
Copier cette balise <bluehtml>link</bluehtml>.
</li>
<li>
Ouvrir le fichier <filestyle>Lesson1.html.lg</filestyle>.
</li>
<li>
Coller le <bluehtml>link</bluehtml> dans le balise <bluehtml>head</bluehtml>.
</li>
<li>
Remplacer <uicpink>maproom:tabterm</uicpink> par <uicpink>term:isDescribedBy</uicpink>.
</li>
<li>
Ajouter la balise <bluehtml>link</bluehtml> suivante dans le <bluehtml>head</bluehtml> de <filestyle>Lesson1.html.lg</filestyle> :
<xmp>
<link rel="term:icon" href="/maproom/icons/Flaticon_5169-32.png" />
</xmp>
</li>
<li>
Copier les deux <bluehtml>fieldsets</bluehtml> suivants :
<xmp>
<fieldset class="navitem" id="toSectionList">
<legend>Maproom</legend>
<a rev="section" class="navlink carryup" href="/maproom/Lesson1/">Leçon 1 lien</a>
</fieldset>
<fieldset class="navitem" id="chooseSection">
<legend about="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Lesson1">
<span property="term:label">Leçon 1 label</span>
</legend>
</fieldset>
</xmp>
</li>
<li>
Les coller dans le <bluehtml>div</bluehtml> avec la classe <uicpink>controlBar</uicpink> dans le fichier <filestyle>Lesson1.html.lg</filestyle>.
</li>
<li>
Répéter toutes les étapes pour le fichier Lesson1.html.en en anglais.
</li>
<li>
Retourner au terminal et construire à nouveau les Maprooms avec la commande <filestyle>make</filestyle>.
</li>
</ol>
</p>

</div>

<div class="rightcol tabbedentries" about="/maproom/Lesson1/" >
<link rel="maproom:tabterm" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Lesson1" />
</div>

</div>
<div class="optionsBar">
            <fieldset class="navitem" id="share"><legend>Partager</legend>

</fieldset>
<fieldset class="navitem langgroup" id="contactus"></fieldset>
</div>
 </body>

 </html>
