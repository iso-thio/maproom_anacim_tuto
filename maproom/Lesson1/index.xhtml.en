<!DOCTYPE HTML>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:wms="http://www.opengis.net/wms#"
      xmlns:iridl="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#"
      xmlns:maproom="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#"
      xmlns:term="http://iridl.ldeo.columbia.edu/ontologies/iriterms.owl#"
      xmlns:xs="http://www.w3.org/2001/XMLSchema#"
      version="XHTML+RDFa 1.0"
 xml:lang="en"
      >
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>How to make a Maproom page and what can I do with it?</title>
<link rel="stylesheet" type="text/css" href="../../uicore/uicore.css" />
<link rel="stylesheet" type="text/css" href="../../localconfig/ui.css" />
<link class="altLanguage" rel="alternate" hreflang="fr" href="index.html?Set-Language=fr" />
<link rel="canonical" href="index.html" />
<link class="carryLanguage" rel="home" href="http://www.anacim.sn/" title="ANACIM" />
<link class="carryLanguage" rel="home alternate" type="application/json" href="/maproom/navmenu.json" />
<meta property="maproom:Sort_Id" content="a01" />
<link rel="term:isDescribedBy" href="http://iridl.ldeo.columbia.edu/ontologies/iridl.owl#climate" />
<link rel="term:icon" href="http://213.154.77.59/SOURCES/.ANACIM/.ENACTS/.version1/.ALL/.monthly/.climatologies/.precip/SOURCES/.Features/.Political/.Senegal_Adm/a:/.Department/.the_geom/:a:/.Region/.the_geom/:a/X/Y/fig-/colors/thin/black/stroke/thinnish/stroke/-fig//antialias/true/psdef//T/6.5/plotvalue+//color_smoothing+1+psdef//plotborder+0+psdef//plotaxislength+432+psdef//antialias+true+psdef+.gif" />
<script type="text/javascript" src="../../uicore/uicore.js"></script>
<script type="text/javascript" src="../../localconfig/ui.js"></script>

<style>
filestyle {font-family:"monospace"; font-size:"11pts";}
dirstyle {font-family:"monospace"; font-size:"11pts"; color: blue}
bluehtml {color: blue;}
greenhtml {color: green;}
darkcyanhtml {color: DarkCyan;}
uicpink {color: HotPink;}
orangehtml {color: orange;}
</style>

</head>
<body>

<form name="pageform" id="pageform">
<input class="carryup carryLanguage" name="Set-Language" type="hidden" />
<input class="carryup titleLink itemImage" name="bbox" type="hidden" />
</form>
<div class="controlBar">
           <fieldset class="navitem">
                <legend>Data Library</legend>
                      <a rev="section" class="navlink carryup" href="/maproom/">My 1st Maproom</a>
            </fieldset>
           <fieldset class="navitem">
                <legend>My 1st Maproom</legend>
                     <span class="navtext">Lesson 1 section</span>
            </fieldset>
            <fieldset class="navitem">
                <legend>Region</legend>
            <a class="carryLanguage" rel="iridl:hasJSON" href="/maproom/senegalregions.json"></a>
                <select class="RegionMenu" name="bbox">
                <option value="">Senegal</option>
                <optgroup class="template" label="Region">
                <option class="irigaz:hasPart irigaz:id@value term:label"></option>
                </optgroup>
                </select>
            </fieldset>

 </div>
<div>
 <div id="content" class="searchDescription">
<h2 property="term:title">How to make a Maproom page and what can I do with it?</h2>
<p align="left" property="term:description">This tutorial will teach how to make a Maproom page and what you can put in it.</p>
<p>
As of now, Lesson 1 is an empty Maproom section, that is to say there are no Maprooms sub-sections or pages in it, and 1 empty tab. However, in <dirstyle>Lesson1/</dirstyle> folder we do have <filestyle>Lesson1.html.lg</filestyle> files. <filestyle>lg</filestyle> denotes a language such as <filestyle>en</filestyle> for English or <filestyle>fr</filestyle> for French and you can choose to work with the language you want.
</p>
<p>
There are three types of files in a Maproom project:
<ul>
<li><filestyle>.xhtml.lg</filestyle> files;</li>
<li><filestyle>.html.lg</filestyle> files;</li>
<li>other files.</li>
</ul>
We will see as we go what the other files are for. <filestyle>.html.lg</filestyle> files are the actual Maproom pages that we are interested in making. However, a Maproom website is virtual, if you wish, before it is built (using the <filestyle>make</filestyle> command). The <filestyle>.xhtml.lg</filestyle> files are responsible for the final structure of the Maproom website as the <filestyle>make</filestyle> pulls information from those files and from the <filestyle>.html.lg</filestyle> files to create new temporary (for this very build) HTML files that will make the Maproom sub-/sections and the navigation links between them. One way to picture it is too consider the <filestyle>.html.lg</filestyle> Maproom pages as the leaves of a tree, and the <filestyle>.xhtml.lg</filestyle> as the trunk and twigs of the tree. Except that the trunk/twigs and leaves structure is not set in stone.
</p>
<p>
This extra step may sound a killer at first but it has several useful purposes:
<ul>
<li>Once you have dozens and dozens of Maproom pages like we do at IRI, this extra step eases the creation of new sub-/sections, pages, or move them around;</li>
<li>Even though the <filestyle>make</filestyle> does set up a final non movable website structure, the whole system allows to make "dynamical" builds. For instance a faceted search interface that will build a Maproom website on the fly, based on user facets choices (e.g. Maprooms about "precipitation" in "Africa"), and on a built-in semantic framework that allows to "tag" Maprooms as such.</li>
</ul>
</p>
<p>
So first of all we need to edit <filestyle>Lesson1.html.lg</filestyle> to have it built properly in our section. Open <filestyle>Lesson1.html.lg with</filestyle> your preferred editor. There are three elements to consider to finalize the building of a Maproom page (or sub-section) into a tab of a section:
<ul>
<li>A semantic tag present in both the <i>parent</i> section in which to build and in the <i>child</i> page/section to be added. Open the <filestyle>index.xhtml.lg</filestyle> file in your editor and search for <uicpink>tabbedentries</uicpink> typically towards the bottom of the file. You will find the following semantic reference:
<xmp>
<link rel="maproom:tabterm" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Lesson1" />
</xmp>
Copy it and go to your <filestyle>Lesson1.html.lg</filestyle> file. Paste it inside the <bluehtml>head</bluehtml> tag and replace <uicpink>maproom:tabterm</uicpink> by <uicpink>term:isDescribedBy</uicpink>.
</li>
<li>Then the <filestyle>make</filestyle> collects three items from the <i>child</i> to build the link to that <i>child</i>: a title, a description and an icon. The <filestyle>make</filestyle> will look for a piece of text in the <i>child</i> that has the tag <orangehtml>property=</orangehtml><uicpink>"term:title"</uicpink>. This is already set up and you can check that the corresponding text is "Lesson 1". The description will be picked up as the piece of text that has the tag <orangehtml>property=</orangehtml><uicpink>"term:description"</uicpink>. This is also already set up. Eventually we need an icon that can be a DL gif or any image. For now we are going to use a non-DL image that we have available in the <dirstyle>icons</dirstyle> folder. Add the following line in <filestyle>Lesson1.html.lg</filestyle> <bluehtml>head</bluehtml> tag:
<xmp>
<link rel="term:icon" href="/maproom/icons/Flaticon_5169-32.png" />
</xmp>
</li>
<li>Two <bluehtml>fieldsets</bluehtml> that will set up in the Maproom Control Bar the navigation back to the <i>parent</i> and the navigation to other Maproom pages in the same section. Those navigation items must be present for the integrity of the website. To set them up, copy the following lines and paste them in the <bluehtml>div</bluehtml> with the class <uicpink>controlBar</uicpink> in your <filestyle>Lesson1.html.lg</filestyle>:
<xmp>
<fieldset class="navitem" id="toSectionList">
<legend>Maproom</legend>
<a rev="section" class="navlink carryup" href="/maproom/Lesson1/">Lesson 1 link</a>
</fieldset>

<fieldset class="navitem" id="chooseSection">
<legend about="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Lesson1"><span property="term:label">Lesson 1 label</span></legend>
</fieldset>
</xmp>
The first <bluehtml>fieldset</bluehtml> legended "Maproom" is setting up a link named "Lesson 1 link" back to <dirstyle>maproom/Lesson1/</dirstyle>. The second <bluehtml>fieldset</bluehtml> uses the same semantic reference we picked up in the <uicpink>tabbedentries</uicpink> and is setting up a menu to navigate through Maprooms of the same section, and labelling this tab "Lesson 1 label".
</li>
</ul>
Before we are ready to build again, we must apply those changes to all the other available languages (here French). Once the file <filestyle>Lesson1.html.fr</filestyle> has been edited accordingly, save your changes (<filestyle>commit</filestyle> and <filestyle>push</filestyle> if you like) and build the Maproom again.
</p>

<h3>Quick Steps</h3>
<p>If you've already been through the lesson, the recap of tasks in Lesson 1.0 is:
<ol>
<li>
Open the <filestyle>index.xhtml.lg</filestyle> file with your preferred editor
</li>
<li>
Search for <uicpink>tabbedentries</uicpink> typically towards the bottom of the file. You will find the following semantic reference:
<xmp>
<link rel="maproom:tabterm" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Lesson1" />
</xmp>
</li>
<li>
Copy the <bluehtml>link</bluehtml>.
</li>
<li>
Open <filestyle>Lesson1.html.lg with</filestyle>.
</li>
<li>
Paste the <bluehtml>link</bluehtml> inside the <bluehtml>head</bluehtml> tag.
</li>
<li>
Replace <uicpink>maproom:tabterm</uicpink> by <uicpink>term:isDescribedBy</uicpink>.
</li>
<li>
Add the following <bluehtml>link</bluehtml> in <filestyle>Lesson1.html.lg</filestyle> <bluehtml>head</bluehtml> tag also:
<xmp>
<link rel="term:icon" href="/maproom/icons/Flaticon_5169-32.png" />
</xmp>
</li>
<li>
Copy both following <bluehtml>fieldsets</bluehtml>:
<xmp>
<fieldset class="navitem" id="toSectionList">
<legend>Maproom</legend>
<a rev="section" class="navlink carryup" href="/maproom/Lesson1/">Lesson 1 link</a>
</fieldset>
<fieldset class="navitem" id="chooseSection">
<legend about="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Lesson1">
<span property="term:label">Lesson 1 label</span>
</legend>
</fieldset>
</xmp>
</li>
<li>
Paste them in the <bluehtml>div</bluehtml> with the class <uicpink>controlBar</uicpink> in the <filestyle>Lesson1.html.lg</filestyle> file.
</li>
<li>
Repeat all steps to the French Lesson1.html.fr files.
</li>
<li>
Go to your terminal and build again the Maprooms with the command <filestyle>make</filestyle>.
</li>
</ol>
</p>

</div>

<div class="rightcol tabbedentries" about="/maproom/Lesson1/" >
<link rel="maproom:tabterm" href="http://iridl.ldeo.columbia.edu/ontologies/maproom.owl#Lesson1" />
</div>

</div>
<div class="optionsBar">
            <fieldset class="navitem" id="share"><legend>Share</legend>

</fieldset>
<fieldset class="navitem langgroup" id="contactus"></fieldset>
</div>
 </body>

 </html>
